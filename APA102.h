#ifndef APA102.h
#define APA102.h

#include <SPI.h>

void startframe(void);
void endframe(void);
void global(int n);
void test_frame(void);

#endif 
