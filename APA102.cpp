/*
 * REGISTRE DE FONCTIONS BIBLIOTHEQUE APA102
 */
#include "APA102.h"
#include "Arduino.h"
#include <stdio.h>
#include <SPI.h>

byte debut = 0x00;
byte fin = 0xFF;

void startframe(void)
{
  //this is to set up the start of your frame
  //4*8Bits at 0 in hexadecimal 0x00
  // after that you control LEDs
  //SPI.begin();
  SPI.write(&debut, 4);
}

void endframe(void)
{
  //this is to set up the end of your frame
  //SPI.begin();
  SPI.write(&fin, 4);
}

//function which set up the frame for luminosity
void global(int n)
{
  //the intensity is set up in 5Bits before colors order
  //32 level of luminosity
  byte value; 
  int i;
    
  //value = light[n];
  for (i = 0; i <= n; i++)
  {
    value = 224 + i ;// return a value for pwm
    if (value > 255)
    {
      value = 255;
    }
  }
  SPI.write(value);
}

/*
void test_frame(void)
{
  //this only for one LED !
  //first Set lumonisity (5bits), after colors (BGR - 8Bits)
  SPI.begin();
}
*/
