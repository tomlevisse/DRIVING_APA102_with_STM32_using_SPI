/*    This code is running on STM32
 *    SPI Pattern communication for adressable LEDs APA102      
 *     
 *     
 *     Using the first SPI port (SPI_1)
    CS    <-->  PA4 <-->  BOARD_SPI1_NSS_PIN
    SCK   <-->  PA5 <-->  BOARD_SPI1_SCK_PIN
    MISO  <-->  PA6 <-->  BOARD_SPI1_MISO_PIN
    MOSI  <-->  PA7 <-->  BOARD_SPI1_MOSI_PIN
*/
//#include <APA102.h>
#include <SPI.h>
#define ENABLE PA4    //SPI_1 Chip Select pin is PA4. You can change it to the STM32 pin you want.

byte startTram = 0x00;
byte endTram = 0xFF;

void setup() {
  // put your setup code here, to run once:
  SPI.begin(); //Initialize the SPI_1 port.
  SPI.setBitOrder(MSBFIRST); // Set the SPI_1 bit order
  SPI.setDataMode(SPI_MODE0); //Set the  SPI_1 data mode 0
  SPI.setClockDivider(SPI_CLOCK_DIV16);      // Slow speed (72 / 16 = 4.5 MHz SPI_1 speed)
  pinMode(ENABLE, OUTPUT);
}

void loop() 
{
  // put your main code here, to run repeatedly:
  digitalWrite(ENABLE, LOW); // manually take CSN low for SPI_1 transmission
  startSPI();
  trame();
  endSPI();
  digitalWrite(ENABLE, HIGH);
  delayMicroseconds(10);    //Delay 10 micro seconds.
}
     
void startSPI()
{
  //start frame 4*8Bits at 0 to initialyse.
  SPI.write(&startTram, 4);
}

void trame()
{
  SPI.write(111); //3Bits at 1
  SPI.write(11111);//code on 5Bits the intensity of LEDs
  SPI.write(0xFF);//BLUE value
  SPI.write(0xFF); //GREEN value
  SPI.write(0xFF);//RED value
  
}

void endSPI()
{
  //finish tram 4*8Bits at 1 to end.
  SPI.write(&endTram, 4);
}